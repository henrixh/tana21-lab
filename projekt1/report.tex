\documentclass[article,a4paper,oneside]{memoir}
\usepackage[swedish]{babel}
\usepackage[utf8]{inputenc}  % UTF8.
\usepackage[T1]{fontenc}     % UTF8.
\usepackage{graphicx}        % Images
\usepackage{amsmath}         % More math
\usepackage{listings}        % Inline code
\usepackage[osf]{mathpazo}
\usepackage{blindtext}
% >> ys = eval_trapezoid(@(p) cos(p),0,2*pi, xs, 0); csvwrite('cosx.csv',ys)
% >> ys = eval_trapezoid(@(p) cos(p),0,2*pi, xs, 0); csvwrite('cosx.csv',ys)
% >> ys = eval_trapezoid(@(p) 4/(1+p^2),0,1, xs, pi); csvwrite('pi.csv',ys)
% >> ys = eval_trapezoid(@(p) sqrt(p),0,1, xs, 2/3); csvwrite('sqrtx.csv',ys)
% >> ys = eval_trapezoid(@(p) 10*p^2 - 5*p + 7,0,1, xs, 47/6); csvwrite('pol1.csv',ys)
% >> ys = eval_trapezoid(@(p) p^5 - p^2 + p,0,1, xs, 1/3); csvwrite('pol2.csv',ys)
% >> ys = eval_trapezoid(@(p) exp(p),0,1, xs, exp(1) - 1); csvwrite('exp.csv',ys)

\counterwithout{section}{chapter}
\author{Mattias Hammar \& Henrik Henriksson}
\title{TANA21 - Miniprojekt 1\\ Trapetsregeln}

\begin{document}
\begin{titlingpage}
\maketitle
\end{titlingpage}
\section{Inledning}
I detta miniprojekt implementeras och testats en Matlab-funktion för numerisk
integration enligt trapetsregeln. Egenskaper hos lösningsalgoritmen, så som
beräkningsfel och beräkningskomplexitet bestäms både teoretiskt och empiriskt.

\section{Uppgift}
Uppgiften är att implementera och evaluera en Matlab-funktion för numerisk
integration genom trapetsregeln. Den implementerade funktionen användes för
att numeriskt beräkna
\begin{align*}
& \int_0^1 e^x dx = e - 1 & 
  & \int_0^1 10x^2 - 5x + 7 dx = \frac{47}{6}  \\
& \int_0^1 x^5 - x^2 + x dx  = \frac{1}{3}&
& \int_0^1 \frac{4}{1 + x^2} = \pi \\
& \int_0^1 \sqrt{x} = \frac{2}{3}&
& \int_0^{2\pi} \cos \omega d\omega = 0.
\end{align*}

För att analysera beräkningskomplexitet och beräkningsfel ska test
konstrueras för att uppskatta noggrannhetsordning och komplexitet, samt
utföras på de ovan nämda funktionerna.

\section{Teori}
Trapetsregeln bygger på att
\begin{align*}
  \int_a^b f(x) &\approx \frac{h}{2} \sum_{k=1}^N \left [ f(x_{k-1}) + f(x_k)]\\
  x_i &= a + ih \\
  h &= \frac{b - a}{N}.
\end{align*}
Utifrån detta följer att
\begin{align*}
  \int_a^b f(x) &\approx h \left[ \left ( \sum_{k=0}^N f(x_k) \right )
                  - \frac{f(x_0) + f(x_N)}{2}\right]
\end{align*}
efter omskrivning.
\subsection{Beräkningskomplexitet}
Givet att $f(x)$ kan beräknas i $O(1))$ är beräkningskomplexiteten för hela
trapetsregeln $O(N)$, ty endast en linjär summation av funktionsvärdena
utförs.

\subsection{Trunkeringsfel}
Det totala trunkeringsfelet vid användning av trapetsregeln ges av
\begin{align*}
  ||R_T|| \approx \left|\frac{b - a}{12}h^2f''(\eta)\right | = O(h^2).
\end{align*}


\section{Lösning}
\label{section:losning}
För att lösa problemet implementerades funktionen \texttt{trapezoid}. Vår
implementation följer den ovanstående summan så nära som möjligt, genom
omskrivningen
\begin{align*}
  \int_a^b f(x) &\approx h \left[ \left ( \sum_{k=0}^N f(x_k) \right )
                  - \frac{f(x_0) + f(x_N)}{2}\right]\\
                &= h \left[ \left(  \sum_{k=0}^N y_i \right )- \frac{f(a) + f(b)}{2} \right] , \, y_i = f(x_i).
\end{align*}
För att sammanställa data användes en separat funktion för att beräkna
integreringens resultat, beräkningsfel och beräkningstid för $\sim50$
logaritmiskt utspridda $1 \leq N \leq 10000$. Denna data sparades sedan ner i
\texttt{.csv} filer som importerades till Wolfram Mathematica, där all faktisk
analys och validering utfördes. För att köra Matlabfunktionerna användes Octave
4.0.0. Matlab användes inte alls. Exakta lösningar av integralerna beräknades
analytiskt i Mathematica.


\section{Kod} % och validering?
Koden skrevs på vektorform, på ett format som är i princip identiskt med
ekvationen i stycke~\ref{section:losning}. Denna kod visas i figur~\ref{fig:code}.

\begin{figure}[ht]
  \centering
\begin{verbatim}
    function res = trapezoid( f, a, b, N)
    res = ((b-a)/N)*(sum(arrayfun(f,linspace(a,b,N+1))) 
          -0.5*(f(a)+f(b)));
\end{verbatim}
  \caption{\label{fig:code} Matlab-kod för trapetsregeln.}
\end{figure}

För att ta fram data för analys användes funktionen \texttt{eval\_trapezoid},
varvid den resulterande matrisen sparades till disk. Notera att koden ger några
dubbletter med $N=1$. Denna kod visas i figur~\ref{fig:evalcode}.
\begin{figure}
  \centering
\begin{verbatim}
    function result = eval_trapezoid(f, a, b, Ns, val)
    result = zeros(size(Ns)(2),5);

    for i = 1:1:size(Ns)(2);
      result(i,1) = floor(Ns(i));
      tic;
      result(i,2) = trapezoid(f,a,b,floor(Ns(i)));
      result(i,4) = toc;
      result(i,3) = abs(val - result(i,2));
      result(i,5) = abs(a-b)/floor(Ns(i));
    end
\end{verbatim}
  \caption{\label{fig:evalcode} Kod för att ta fram data kring integreringen automatiskt.}
\end{figure}

Man påvisade att koden var ``relativt rätt'' genom manuella tester med högre $N$
och jämförelser med analytiska värden av funktionerna ovan.


\section{Resultat och svar}
I figur~\ref{fig:timeplot} visas hur beräkningstiden beror av antalet steg i
approximationen. Att detta samband är linjärt kan ses i figuren.
Följaktligen följer den empiriskt uppmätta tidskomplexiteten den
teoretiskt beräknade linjära tidskomplexiteten.
\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{timeplot.pdf}
  \caption{\label{fig:timeplot} Körtid relaterat antal steg för de olika funktionerna. }
\end{figure}

Det absoluta felet beroende av antal steg $N$ visas i figur~\ref{fig:errorplot}
för de olika funktionerna. På denna data utfördes även en uppskattning av
felkomplexiteten. Antag att $||R_T \approx c\dot h^p$. Med hjälp av denna
approximation kunde värdena i tabell~\ref{tab:errorvals} beräknas via
Mathematicas \texttt{FindFit}-funktion.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{errorplot.pdf}
  \caption{\label{fig:errorplot} Absolut fel beroende av antalet
    integrationsintervall för de olika funktionerna. }
\end{figure}


\begin{table}
  \caption{\label{tab:errorvals} Uppskattning av felkomplexiteten för de
    olika funktionerna, givet en tidskomplexitet på formen $||R_T|| \approx c\cdot h^p$. }
  \centering
\[\begin{array}{lll}
 e^x & c\to 0.140864 & p\to 1.98448 \\
 7-5 x+10 x^2 & c\to 1.66667 & p\to 2. \\
 x-x^2+x^5 & c\to 0.167037 & p\to 1.62361 \\
 \frac{4}{1+x^2} & c\to 0.141712 & p\to 1.8109 \\
 \sqrt{x} & c\to 0.166739 & p\to 1.41173 \\
 \text{Cos}[x] & c\to 0.0000103206 & p\to 7.24638 \\
\end{array}\]
\end{table}

\section{Diskussion}
Trapetsregeln kan implementeras enkelt och relativt effektivt, med goda
resultat. De erhållna värdena för $p$ skiljer sig något från teorin, men inte på
något drastiskt sätt. Tidskomplexiteten är, precis som förväntat, linjär
relativt antalet intervall.

De flesta värden på $p$ från tabell~\ref{tab:errorvals} ligger kring $2$, men
några uppskattningar av $p$ sticker ut. $\cos(x)$ får en mycket bra uppskattning
efter bara ett fåtal iterationer, och får sedan en gradvis sämre uppskattning
med fler iterationer, vilket tydligt ses i figur~\ref{fig:errorplot}. Detta
beror på att $\cos x$ är en periodisk funktion, som även innehar en viss
symmetri kring båda axlar. I fallet $\cos x$ kommer därmed felen kring
intervallets mittpunkt, och tillika nollställe, ta ut varandra, varvid ett värde
nära det analytiska erhålles. Att felet sedan långsamt ökar med antal steg beror
troligtvis på ökade fel i flyttalsberäkningarna.

Approximationernas värden för olika steglängder anses inte vara relevant att
visa i varken tabell- eller grafform, då detta enbart skulle tillföra redundant
information.


\end{document}
