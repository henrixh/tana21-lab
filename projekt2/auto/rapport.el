(TeX-add-style-hook
 "rapport"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("memoir" "article" "a4paper" "oneside")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "swedish") ("inputenc" "utf8") ("fontenc" "T1") ("mathpazo" "osf")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "latex2e"
    "memoir"
    "memoir10"
    "babel"
    "inputenc"
    "fontenc"
    "graphicx"
    "amsmath"
    "listings"
    "mathpazo")
   (LaTeX-add-labels
    "eq:iter"
    "fig:code"
    "fig:modded"
    "fig:f1"
    "fig:f2"
    "fig:f3"
    "fig:f4"))
 :latex)

