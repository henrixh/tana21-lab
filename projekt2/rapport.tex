\documentclass[article,a4paper,oneside]{memoir}
\usepackage[swedish]{babel}
\usepackage[utf8]{inputenc}  % UTF8.
\usepackage[T1]{fontenc}     % UTF8.
\usepackage{graphicx}        % Images
\usepackage{amsmath}         % More math
\usepackage{listings}        % Inline code
\usepackage[osf]{mathpazo}

\lstdefinestyle{lol}{
  belowcaptionskip=1\baselineskip,
  frame=L,
  numbers=left,
  numberstyle=\tiny,
  xleftmargin=\parindent,
  language=Python,
  showstringspaces=false,
  basicstyle=\scriptsize\ttfamily,
  keywordstyle=\bfseries,
  commentstyle=\itshape,
}

\lstset{style=lol}

\lstset{
  literate={ö}{{\"o}}1
           {ä}{{\"a}}1
           {ü}{{\"u}}1
}

\counterwithout{section}{chapter}
\author{Mattias Hammar \& Henrik Henriksson}
\title{TANA21 - Miniprojekt 2\\ Sekantmetoden}

\begin{document}
\begin{titlingpage}
\maketitle
\end{titlingpage}
\section{Inledning}
I denna rapport implementeras en Matlab-funktion för att numeriskt hitta
nollställen för icke-linjära funktioner baserat på sekantmetoden.
Konvergensordningen för implementationen tas sedan fram experimentellt.

\section{Metod}
Baserat på två framgissade startvärden $x_0, x_1$ kommer sekantmetoden iterativt
ta fram bättre gissningar av ett nollställe mellan de initiala gissningarna.
För att ta fram en bättre gissning tar man fram
\begin{align}
  \label{eq:iter}
  x_{n+1} = x_n - \frac{x_n - x_{n-1}}{f(x_n) - f(x_{n - 1})} f(x_n).
\end{align}
Detta kan liknas med att man drar en linje mellan de två senaste gissningarna
och gissar på denna linjes nollskärning.

Konvergensordningen för sekantmetoden $p$ är gyllene snittet,
\begin{align*}
  p = \frac{1 + \sqrt{5}}{2}.
\end{align*}
Roten bör vara enkel och funktionen $f$ som metoden appliceras på måste ha en
kontinuerlig andraderivata för att denna konvergensordning ska gälla.
Ytterligare behöver de initiala gissningarna vara ``tillräckligt bra'', där
``tillräckligt bra'' är baserat på hur funktionen förändras i intervallet $[x_0,
x_1]$.

\subsection{Analys}
För analys av konvergensordningen användes funktionerna
\begin{align*}
  f_1(x) &= x^2 - 10, & &f_1(\pm \sqrt{10}) = 0 \\
  f_2(x) &= x^3 + x^2 - x - 1, & &f_2(\pm 1) = 0 \\
  f_3(x) &= x^2, &      &f_3(0) = 0 \\
  f_4(x) &= \sin x, & &f_4(n\pi) = 0,\, \forall n \in \mathbb{Z}.
\end{align*}

All kod kördes i Octave, varvid resultat exporterades till \texttt{.csv} filer
för vidare analys i Wolfram Mathematica. \footnote{Ingendera författare
  uppskattar den misshandel av god språkdesign som Matlab utgör, utan önskar
  istället att Python, Mathematica, eller annat rimligt system, vore
  branchstandard.} För att beräkna konvergensordningen $p$ används sambandet
\begin{align*}
  \lim_{n\rightarrow\infty} \frac{|\epsilon_{n+1}|}{|\epsilon_n|^p} &= C,\\
  \lim_{n\rightarrow \infty} x_n &= x, \\
  \epsilon_n &= x_n - x,
\end{align*}
där $x$ betecknar det exakta värdet och $C$ är konstant. Utifrån detta kan man
visa att
\begin{align*}
  |\epsilon_{n+1}| \approx C|e_n|^p.
\end{align*}
Detta samband kan användas för att empiriskt bestämma $C$ och $p$. Sambandet
plottades för de fyra funktionerna, nästkommande värde plottades alltså som en
funktion av nuvarande värde. Då graferna är $\log\log$-plottar kan man sedan
använda ``linjalen på pappret'', alltså plocka ut en ungefärlig lutning av
funktionen genom att dra en rät linje. Denna lutning utgör sedan
konvergensordningen $p$. Värdet på $p$ bestämdes genom att matcha en linjes
lutning mot den aktuella funktionen.


\subsection{Insamling av data}
Data samlades in genom att köra funktionen över relativt dåliga gissningar fram
till ett litet tröskelvärde. Vid varje iteration sparades $x_i$ undan, och
utifrån detta togs en uppskattning av konvergensordningen fram.
Matlab-funktionen modifierades alltså till att inte bara ge ett approximativt
nollställe, utan en matris med samtliga besökta punkter på vägen. Som brytpunkt
för att avbryta sökningen användes $10^{-13}$, en liten siffra som fortfarande
är betydligt större än maskinepsilon. I praktiken fungerade även $0$ som
brytpunkt. 

\section{Resultat}
\subsection{Kod}
Sekantmetoden implementerades enligt koden i figur~\ref{fig:code}. Koden
itererar över $x_i$ enligt ekvation~(\ref{eq:iter}) till dess att ett visst
tröskelvärde \texttt{threshold} uppnåtts. För att ta fram data för att bestämma
nogrannhetsordningen modifierades koden till den implementation som visas i
figur~\ref{fig:modded}, vilken returnerar en lista på samtliga besökta punkter.

\begin{figure}
  \centering
  \begin{lstlisting}
function res = mysol( f, x0, x1, threshold)
while abs(x1 - x0) > threshold;
    temp = x1;
    x1 = x1 - f(x1) * (x1 - x0)/(f(x1) - f(x0));
    x0 = temp;
end
res = x1
end
\end{lstlisting}
  \caption{\label{fig:code} Matlab-funktion för att hitta nollställen med sekantmetoden. }
\end{figure}

\begin{figure}
  \centering
  \begin{lstlisting}
function res = mysol( f, x0, x1, threshold)
  res = [];
while abs(x1 - x0) > threshold;
  res(end+1) = x0;
    temp = x1;
    x1 = x1 - f(x1) * (x1 - x0)/(f(x1) - f(x0));
    x0 = temp;
end
res(end+1) = x1;
end
\end{lstlisting}
  \caption{\label{fig:modded} Matlab-funktion för att hitta nollställen med
    sekantmetoden. Denna version returnerar samtliga besökta punkter $x_i$, men
    är i övrigt identisk. }
\end{figure}

\subsection{Analys}
Konvergensordningen bestämdes till $p=1.681$ för $f_1, f_2$, $p=1$ för $f_3$ och
$p=2$ för $f_4$, se figur~\ref{fig:f1}, \ref{fig:f2}, \ref{fig:f3} och \ref{fig:f4}.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{f1.eps}
  \caption{\label{fig:f1} Bestämning av $p$ för $f_1(x)$} 
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{f2.eps}
  \caption{\label{fig:f2} Bestämning av $p$ för $f_2(x)$} 
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{f3.eps}
  \caption{\label{fig:f3} Bestämning av $p$ för $f_3(x)$} 
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{f4.eps}
  \caption{\label{fig:f4} Bestämning av $p$ för $f_4(x)$} 
\end{figure}


\section{Diskussion}
Konvergensordningen stämmer väl överens med det teoretiska värdet för de två
polynom som har enkelrötter. I fallet med enkelrot blir konvergensordningen
istället linjär, något som även det stämmer väl överens med teoretiska värden.
För den periodiska $f_4$ blir konvergensordningen 2, detta beror på att $\sin x$
är approximativt linjär nära nollställen. Flera olika begynnelsevärden testades,
men dessa gav väldigt liknande resultat, givet att de konvergerade.

\end{document}