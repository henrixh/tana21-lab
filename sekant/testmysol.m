



f1 = @(x) x^2 - 10;
f2 = @(x) x^4 + x - 20;
f3 = @(x) x^2;
f4 = @(x) sin(x);
e = 0.00000001;
x0 = -200;
x1 = 300;

csvwrite('f1.csv', mysol(f1, x0 ,x1, e));
csvwrite('f2.csv', mysol(f2, x0, x1, e));
csvwrite('f3.csv', mysol(f3, x0, x1, e));
csvwrite('f4.csv', mysol(f4, x0, x1, e));

