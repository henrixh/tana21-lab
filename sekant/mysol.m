function res = mysol( f, x0, x1, threshold)
%MYSOL Summary of this function goes here
%   Detailed explanation goes here

while abs(x1 - x0) > threshold;
    temp = x1;
    x1 = x1 - f(x1) * (x1 - x0)/(f(x1) - f(x0));
    x0 = temp;
end
res = x1
end

