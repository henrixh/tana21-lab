function res = trapezoidz(f,a,b,N)
% Approximates integral with trapezoidz formula.
% (same as trapezoid formula, but with matrix implementation)

  xs = linspace(a, b, N);
  fxs = arrayfun(f, xs);
  xs = linspace(a, b, N);
  fxs = arrayfun(f, xs);
  fxs(1) *= 0.5;
  fxs(end) *= 0.5;  
  res = (b - a) * sum(fxs) / N;
  
end