function res = trapezoid( f, a, b, N)
res = ((b-a)/N)*(sum(arrayfun(f,linspace(a,b,N+1))) - 0.5*(f(a)+f(b)));