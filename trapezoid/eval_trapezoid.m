function result = eval_trapezoid(f, a, b, Ns, absval)
% Approximates an integral with the trapezoid formula
% 

result = zeros(size(Ns)(2),5);

for i = 1:1:size(Ns)(2);
result(i,1) = floor(Ns(i));
tic;
result(i,2) = trapezoid(f,a,b,floor(Ns(i)));
result(i,4) = toc;
result(i,3) = abs(absval - result(i,2));
result(i,5) = abs(a-b)/floor(Ns(i));
end